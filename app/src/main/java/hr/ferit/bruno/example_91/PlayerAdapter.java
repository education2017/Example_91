package hr.ferit.bruno.example_91;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Stray on 19.9.2017..
 */

public class PlayerAdapter extends BaseAdapter{

	private PlayerDatabase mPlayerDatabase;
	private List<Player> mPlayerList;

	public PlayerAdapter(Context context){
		this.mPlayerDatabase = PlayerDatabase.getInstance(context);
		this.mPlayerList = this.mPlayerDatabase.playerDao().getAllPlayers();
	}

	@Override
	public int getCount() {
		return this.mPlayerList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.mPlayerList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PlayerViewHolder holder;

		if(convertView == null){
			convertView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.list_item_player, parent, false);
			holder = new PlayerViewHolder(convertView);
			convertView.setTag(holder);
		}else{
			holder = (PlayerViewHolder) convertView.getTag();
		}

		Player player = this.mPlayerList.get(position);
		holder.tvPlayerName.setText(player.getName());
		holder.tvPlayerPoints.setText(String.valueOf(player.getPoints()));
		holder.tvPlayerGold.setText(String.valueOf(player.getGold()));

		return convertView;
	}

	public void addPlayer(Player player) {
		this.mPlayerDatabase.playerDao().insertPlayer(player);
		this.refreshData();
	}

	private void refreshData() {
		this.mPlayerList = this.mPlayerDatabase.playerDao().getAllPlayers();
		this.notifyDataSetChanged();
	}

	public void removeUser(int position) {
		this.mPlayerDatabase.playerDao().deletePlayer(this.mPlayerList.get(position));
		this.refreshData();
	}

	static class PlayerViewHolder{
		@BindView(R.id.tvPlayerName) TextView tvPlayerName;
		@BindView(R.id.tvPlayerGold) TextView tvPlayerGold;
		@BindView(R.id.tvPlayerPoints) TextView tvPlayerPoints;

		public PlayerViewHolder(View view){
			ButterKnife.bind(this, view);
		}
	}
}
