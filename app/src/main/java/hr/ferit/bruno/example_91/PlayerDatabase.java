package hr.ferit.bruno.example_91;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by Zoric on 19.9.2017..
 */

@android.arch.persistence.room.Database(entities={Player.class}, version=1)
public abstract class PlayerDatabase extends RoomDatabase{

    protected static PlayerDatabase mPlayerDatabase = null;

    public static PlayerDatabase getInstance(Context context){
        if(mPlayerDatabase == null){
            context = context.getApplicationContext();
            mPlayerDatabase = Room.databaseBuilder(
                    context, PlayerDatabase.class,Schema.DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return mPlayerDatabase;
    }

    public abstract PlayerDao playerDao();

    static class Schema{
        private static final String DATABASE_NAME = "database";
    }
}
