package hr.ferit.bruno.example_91;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.bAddUser) Button bAddUser;
    @BindView(R.id.lvUsers) ListView lvUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        PlayerAdapter adapter = new PlayerAdapter(this);
        this.lvUsers.setAdapter(adapter);
    }

    @OnClick(R.id.bAddUser)
    public void addUserClicked(){
        PlayerAdapter adapter = (PlayerAdapter) this.lvUsers.getAdapter();
        Player player = new Player();
        player.setName("Bruno");
        player.setGold(100);
        player.setPoints(90);
        adapter.addPlayer(player);
    }

    @OnItemLongClick(R.id.lvUsers)
	public boolean removeUserLongClick(int position){
		PlayerAdapter adapter = (PlayerAdapter) this.lvUsers.getAdapter();
		adapter.removeUser(position);
		return true;
	}
}
