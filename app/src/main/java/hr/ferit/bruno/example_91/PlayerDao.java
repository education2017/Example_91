package hr.ferit.bruno.example_91;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.sqlite.SQLiteQueryBuilder;

import java.util.List;

/**
 * Created by Zoric on 19.9.2017..
 */

@Dao
public interface PlayerDao {
    @Query("SELECT _id, name, points, gold FROM players")
    List<Player> getAllPlayers();

    @Insert
    void insertPlayer(Player player);

    @Delete
    void deletePlayer(Player player);
}
