package hr.ferit.bruno.example_91;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Zoric on 19.9.2017..
 */

@Entity(tableName = "players")
public class Player {
    @ColumnInfo(name="_id") @PrimaryKey(autoGenerate = true) private int mID;
    @ColumnInfo(name = "name") private String mName;
    @ColumnInfo(name = "points") private int mPoints;
    @ColumnInfo(name = "gold") private int mGold;

    public int getID() {
        return mID;
    }

    public String getName() {
        return mName;
    }

    public int getPoints() {
        return mPoints;
    }

    public int getGold() {
        return mGold;
    }

    public void setID(int ID) {
        mID = ID;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setPoints(int points) {
        mPoints = points;
    }

    public void setGold(int gold) {
        mGold = gold;
    }
}
